#!/bin/bash
# I use this script to build and publish deb packages in ppa:mikhailnov/utils (https://launchpad.net/~mikhailnov/+archive/ubuntu/utils)
# I publish it to allow other people to use it and make it possible to maintain a new PPA easily in case I stop doing it for some reason
# I think, it can also be used for maintaining packages in mainline Debian (minor modifications required)

build_package(){
	pkg_version="$(echo "$directory" | rev | cut -d'-' -f1 | rev)"
	pkg_name="$(echo "$directory" | sed -e "s/${pkg_version}$//g" -e "s/-$//g")"

	debian/rules clean
	dir0="$(pwd)"

	for i in bionic cosmic disco eoan
	do
		old_header=$(head -1 ./debian/changelog)
		old_version="$(cat ./debian/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
		new_version="${old_version}~${i}1"
		sed -i -re "s/${old_version}/${new_version}/g" ./debian/changelog
		sed -i -re "1s/unstable/$i/" ./debian/changelog
		# -I to exclude .git; -d to allow building .changes file without build dependencies installed
		dpkg-buildpackage -I -S -sa -d
		sed  -i -re "1s/.*/${old_header}/" ./debian/changelog
		cd ..
		
		# change PPA names to yours, you may leave only one PPA; I upload to 2 different PPAs at the same time
		for ppa_name in ppa:mikhailnov/utils
		# I can copy from desktop1-dev to utils at Launchpad
		do
			dput -f "$ppa_name" "${pkg_name}_${new_version}_source.changes"
		done
		
		cd "$dir0"
		sleep 1
	done

	debian/rules clean
	#cd "$dir_start"
}

dir_start="$(pwd)"
for directory in "libtgvoip-2.4.4" "xxhash-0.7.0" "telegram-desktop-1.7.9"
do
	cd "$directory"
	build_package #"$directory"
	cd "$dir_start"
done
