libtgvoip (2.4.4-2) unstable; urgency=medium

  * Rebuild v2.4.4 for Ubuntu 18.04-19.10

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sun, 23 Jun 2019 22:26:11 +0300

libtgvoip (2.4.4-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 01 Apr 2019 22:57:35 +0300

libtgvoip (2.4.2-1) unstable; urgency=medium

  * Rebuild libtgvoip 2.4.2-1 from Debian Sid for Ubuntu 18.04, 18.10 and 19.04
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Tue, 16 Apr 2019 20:27:00 +0300

libtgvoip (2.4.2-1) unstable; urgency=medium

  * New upstream release.
  * Update patches:
     - Reintroduce Disable-SSE2.patch to avoid SSE2 code on i386
     - Fix-WebRTC-for-non-Linux.patch allows building the package on
       kFreeBSD or GNU/Hurd system.
     - Pointer-to-ServerConfigImpl.patch hides implementation details of
       ServerConfig class.
     - Drop Port-WebRTC.patch as applied by upstream (partially).

 -- Nicholas Guriev <guriev-ns@ya.ru>  Tue, 29 Jan 2019 23:41:52 +0300

libtgvoip (2.4-4) unstable; urgency=medium

  * Rebuild libtgvoip 2.4-2 from Debian Sid for Ubuntu 18.04, 18.10 and 19.04
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Tue, 01 Jan 2019 14:37:00 +0300

libtgvoip (2.4-2) unstable; urgency=medium

  * Fix porting issues (closes: #917315).
    - Add Port-WebRTC.patch instead Disable-SSE2-on-i386.patch.
  * Bump standards version to 4.3.0, no related changes.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sat, 29 Dec 2018 23:39:41 +0300

libtgvoip (2.4-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 19 Dec 2018 00:47:38 +0300

libtgvoip (2.2.4-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 01 Oct 2018 09:10:30 +0300

libtgvoip (2.2.3+git20180828.31fe4af-1) unstable; urgency=medium

  * New upstream release along with latest fixes.
  * Drop Fix-build-on-FreeBSD-and-Hurd.patch as applied by upstream.
  * Bump standards version to 4.2.1, no related changes.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Fri, 31 Aug 2018 08:19:46 +0300

libtgvoip (2.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards version to 4.1.5.
     - Add the Rules-Requires-Root field into the debian/control file.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 09 Jul 2018 20:38:35 +0300

libtgvoip (2.1-1) unstable; urgency=medium

  * New upstream release of static library.
    - There is no shared variant of the library because this C++ code is not
      intended to be used as .so file.
  * Bump debhelper version to 11, no changes for this.
  * Bump standards version to 4.1.4, no changes for this.
  * Refine the Built-Depends list.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Fri, 15 Jun 2018 10:33:22 +0300

libtgvoip (1.0.3-3) unstable; urgency=medium

  * Disable SSE code to fix FTBFS on i386

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 28 Mar 2018 23:25:16 +0300

libtgvoip (1.0.3-2) unstable; urgency=medium

  * Clarify copyright notices about WebRTC code (closes: #892799)
  * Remove -msse2 compiler flag from build scripts (closes: #892823)
  * Hide private symbols in result binary
  * Refine debian/symbols file for 32-bit architectures (closes: #892824)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 26 Mar 2018 08:48:49 +0300

libtgvoip (1.0.3-1) unstable; urgency=medium

  * New upstream release
  * Move repository of the package to salsa.debian.org
  * Create debain/watch file for tracking upstream versions
  * Bump soname version because of new C++ destructors
  * Update standards version, no changes for this
  * Add d/symbols file
  * Update year in d/copyright

 -- Nicholas Guriev <guriev-ns@ya.ru>  Mon, 12 Mar 2018 09:15:50 +0300

libtgvoip (1.0~git20170704.445433f-4) unstable; urgency=medium

  * Avoid unconditional using MAXPATHLEN macro for build on GNU/Hurd
  * Bump Standards-Version, no changes

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 17 Sep 2017 20:57:54 +0300

libtgvoip (1.0~git20170704.445433f-3) unstable; urgency=medium

  * Add a patch to fix FTBFS on GNU/Hurd (closes: #870412)
  * Remove the libssl1.0-dev build dependency (closes: #870778)
    - No modifications for this

 -- Nicholas Guriev <guriev-ns@ya.ru>  Thu, 17 Aug 2017 22:54:05 +0300

libtgvoip (1.0~git20170704.445433f-2) unstable; urgency=medium

  * Add a patch to fix FTBFS on GNU/kFreeBSD (closes: #863116)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 30 Jul 2017 11:12:23 +0300

libtgvoip (1.0~git20170704.445433f-1) unstable; urgency=medium

  * Update to the latest commit; new soname
  * Bump Standards-Version
  * Add patch to fix FTBFS with OpenSSL 1.1

 -- Nicholas Guriev <guriev-ns@ya.ru>  Tue, 04 Jul 2017 23:28:15 +0300

libtgvoip (0.4.1~git20170517.2ed5a50-1) unstable; urgency=low

  * Initial upload (closes: #862582)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 17 May 2017 17:06:45 +0300
